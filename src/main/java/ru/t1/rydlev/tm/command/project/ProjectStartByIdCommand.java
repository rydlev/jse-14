package ru.t1.rydlev.tm.command.project;

import ru.t1.rydlev.tm.enumerated.Status;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public String getDescription() {
        return "Start project by id.";
    }

    @Override
    public String getName() {
        return "project-start-by-id";
    }

}
