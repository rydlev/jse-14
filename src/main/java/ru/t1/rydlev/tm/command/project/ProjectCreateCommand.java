package ru.t1.rydlev.tm.command.project;

import ru.t1.rydlev.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getProjectService().create(name, description);
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public String getName() {
        return "project-create";
    }

}
