package ru.t1.rydlev.tm.command.task;

import ru.t1.rydlev.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getTaskService().create(name, description);
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public String getName() {
        return "task-create";
    }

}
