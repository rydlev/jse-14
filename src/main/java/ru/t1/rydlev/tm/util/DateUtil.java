package ru.t1.rydlev.tm.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    SimpleDateFormat TO_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    static String formatDate(Date date) {
        return TO_FORMAT.format(date);
    }

}
