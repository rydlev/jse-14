package ru.t1.rydlev.tm.exception.entity;

import ru.t1.rydlev.tm.exception.AbstractException;

public class AbstractEntityException extends AbstractException {

    public AbstractEntityException() {
    }

    public AbstractEntityException(String s) {
        super(s);
    }

    public AbstractEntityException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public AbstractEntityException(Throwable throwable) {
        super(throwable);
    }

    public AbstractEntityException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }

}
